from inspect import stack
from colorama import Fore, Back, Style

def endline(*args):
    # Prints in a special format
    statements = []
    for arg in args:
        if type(arg) == str:
            statements.append(arg.upper())
        else:
            statements.append(arg)

    print("\t", Fore.LIGHTWHITE_EX, Back.RED, "^^^^^^^^^^ ", *statements, " ^^^^^^^^^^", Style.RESET_ALL, "\n")

def debug(*args, title=None):
    # Prints in a special format to more easily find a printed statement for debugging
    # The stack portion grabs the line number in the code of the printed message and the file name
    # print(dir(stack()[1].frame))
    scope = stack()[1]
    lineno = scope.lineno
    filename = scope.filename
    if title == None:
        print("\n\t", Fore.RED, "\033[4m", "LINE NUMBER: {} | FILE NAME: {}".format(lineno, filename), Style.RESET_ALL, "\n", Fore.RED, *args, Style.RESET_ALL, "\n")
    else:
        print("\n", Fore.RED, "\033[4m{}\tLINE NUMBER: {} | FILE NAME: {}".format(title, lineno, filename), Style.RESET_ALL, "\n", Fore.RED, *args, Style.RESET_ALL, "\n")

def title(*args):
    # Prints in a special format
    statements = []
    for arg in args:
        if type(arg) == str:
            statements.append(arg.upper())
        else:
            statements.append(arg)

    print("\n\t", Style.BRIGHT, Fore.WHITE, Back.GREEN, "-----=====", *statements, " =====-----", Style.RESET_ALL)

def startup(*args):
    print(Fore.YELLOW, *args, Style.RESET_ALL)

# def long(string):
#     prev_char_was_space = False
#     prev_char_was_nl = False
#     for char in string:
#         if char == "\n":
#             if prev_char_was_nl == False:
#                 char = ""
#             prev_char_was_nl = True
#         else:
#             prev_char_was_nl = False
#             if char == " ":
#                 if prev_char_was_space == False:
#                     char = ""
#                 prev_char_was_space = True
#             else:
#                 prev_char_was_space = False
#     return string
#
# def long(str):
#     string = str
#     prev_char_was_space = False
#     prev_char_was_nl = False
#     for char in string:
#         if char == "\n":
#             if prev_char_was_nl == False:
#                 char = ""
#             prev_char_was_nl = True
#         else:
#             prev_char_was_nl = False
#             if char == " ":
#                 if prev_char_was_space == False:
#                     char = ""
#                 prev_char_was_space = True
#             else:
#                 prev_char_was_space = False
#     return string

if __name__ == "__main__":
    endline("End line test", "big boy", type("yes sir"), 12)
    debug("Just you know, testing the debug command right now", title="Title")
    debug("Just you know, testing the debug command right now")
    title("Title test")
    startup("This is some start up text, beep boop")
#     print(
#     '''
#         *So, on the PV Support Group server we use roles to tag people with
#         what classes they have.* Why would we do this? Because you can mention
#         everyone with a role by doing **@RoleName**
#
#         Example (it wont work in DMs bc there aren't any roles here):
#         @algebra II – robertson.
#     '''
#     )
#     print(
#     long(
#     '''
#         *So, on the PV Support Group server we use roles to tag people with
#         what classes they have.* Why would we do this? Because you can mention
#         everyone with a role by doing **@RoleName**
#
#         Example (it wont work in DMs bc there aren't any roles here):
#         @algebra II – robertson.
#     '''
# )
# )

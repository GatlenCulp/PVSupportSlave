import json
import pretty_printing as p
from settings import json_is_condensed

# Open links.json
with open("JSON-Data/links.json", "r") as f:
    links_json = json.load(f)
p.startup("links.json imported")

def sync_links_json():
    with open("JSON-Data/links.json", "w") as fw:
        if json_is_condensed:
            json.dump(links_json, fw, separators=(",", ":"))
        else:
            json.dump(links_json, fw, indent=2)

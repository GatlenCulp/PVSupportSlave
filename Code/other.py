from colorama import Fore, Back, Style

def send_message_to_console(message):
    if message.server == None:
        print(Fore.CYAN, "[MESSAGE] ", message.channel, " > ",
        message.author.name, ": \"",
        message.content, "\"", Style.RESET_ALL)
    else:
        print(Fore.CYAN,"[MESSAGE] ", message.server.name, " > ",
        message.channel.name, " > ", message.author.name, ": \"",
        message.content, Style.RESET_ALL)

def role_names_to_roles(server, *args):
    # If the name of a role in the server matches the role mentioned, return
    # the role
    roles_converted = []
    for role_name in args:
        role_name = role_name.replace(" ", "").upper()
        for role in server.roles:
            if role_name == role.name.replace(" ", "").upper():
                roles_converted.append(role)
    return roles_converted

def role_ids_to_roles(server, *role_ids):
    # If the ID of a role in the server matches the role mentioned, return
    # the role
    roles_converted = []
    for role_id in role_ids:
        for role in server.roles:
            if role_id == role.id:
                roles_converted.append(role)
    return roles_converted

def if_roles_below_position(roles, pos):
    if roles == []:
        return False
    for role in roles:
        if role.position > (pos.position-1):
            return False
    return True

def find_different_item(list1, list2):
    set1 = set(list1)
    set2 = set(list2)
    longest_set = max(set1, set2, key=len)
    shortest_set = min(set1, set2, key=len)
    for item in longest_set:
        if item in shortest_set:
            continue
        else:
            return item

from settings import color

def is_a(role, type):
    role_color = role.color.value
    if type == "subject":
        return (role_color == color['8'] or role_color == color['9'] or role_color == color['10'] or
        role_color == color['11'] or role_color == color['12'])
    else:
        # Used for detecting if a role is a certain class role, club role, subject role, etc
        try:
            return role_color == color[type]
        except KeyError:
            return

def has_class_role(aft):
        # Make sure they have a class role, if not, don't do this. Wait for the day to be over for them to get
        # Pinged about the role
        for role in aft.roles:
            # Okay, they pass
            if is_a(role, 'class'):
                return role
            # NAY, LEAVE
            elif role == aft.roles[-1]:
                return False

def index_plus(list, item):
    try:
        return list.index(item)
    except ValueError:
        return -1

import datetime, math

def class_to_grade(class_role):
    if class_role:
        if isinstance(class_role, int) is not True:
            class_year = int(class_role.name[8:13])
        else:
            class_year = class_role
        grad_date = datetime.date(class_year, 6, 1)
        today = datetime.date.today()
        time_til_grad = grad_date - today
        grade = str(12 - math.floor((time_til_grad.days/365)))
        return grade
    else:
        return None

import json
import pretty_printing as p
from colorama import Fore, Back, Style

# Open teachers.json
with open("JSON-Data/teachers.json", "r") as f:
    teachers_json = json.load(f)
p.startup("teachers.json imported")

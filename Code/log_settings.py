import datetime, logging, os
from colorama import Fore, Back, Style

logger_enabled = False

# Creating root logger and script logger
if logger_enabled:
    root_log_level = logging.INFO
    # Making a folder with the date as the name
    time_ran = str(datetime.datetime.now())
    log_folder = "./logs/{time_ran} (running)".format(time_ran=time_ran)
    os.makedirs(log_folder)
    # Setting up root log
    logging.basicConfig(filename=log_folder + "/root.log",
                        format="%(name)s: %(filename)s on %(lineno)d @ %(asctime)s\
                        \n\t%(levelname)s: %(message)s",
                        level=root_log_level)
    # Other stuff
    logging.info("The log level is set at {level}".format(level=root_log_level))
    logger = logging.getLogger("main")
    print(logger)
    print(Fore.YELLOW, "Logger created and configured", Style.RESET_ALL)

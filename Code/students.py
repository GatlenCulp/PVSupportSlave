import atexit, json, logging, uuid
from other import is_a, index_plus
from settings import json_is_condensed

# Json of all the students
with open("JSON-Data/students.json", "r") as f:
    students_json = json.load(f)

def Student(discord=None, phone=None, email=None):
    # Weird ass dictionary pseudoclass that creates a student for JSON database
    # Ensure these arguments aren't empty
    if any((discord, phone, email)):
        # See if they are already in the system first
        for student in students_json:
            if (student['contacts']['discord'] == discord and
                student['contacts']['phone'] == phone and
                student['contacts']['email'] == email):
                print("DUPLICATE ALERT! DUPLICATE ALERT FOR", discord)
                return
        # If they aren't in the system, add them
        # Create their contacts
        contacts = {'discord': discord, 'phone': phone, 'email': email}
        # Add them to the database
        output = {
            "uid": str(uuid.uuid4()),
            "contacts": contacts,
            "classes": {
                "8": [],
                "9": [],
                "10": [],
                "11": [],
                "12": []
            },
            "class": None
        }
        students_json.append(output)
        return output
    # The args were empty, so print an error (one arg is needed)
    else:
        print("ERROR: You must include a discord, phone or email when initializing a student")

def add_role(id, role, grade=None):
    for student in students_json:
        # Find the user based on Discord ID
        if student['contacts']['discord'] == id:
            # If it is a subject, add it to their grade
            if is_a(role, "subject"):
                name = role.name.upper()
                # Make sure there are no duplicates and grade is given
                if index_plus(student['classes'][grade], name) is -1 and grade != None:
                    student['classes'][grade].append(name)
            # If it is their class, add it to their class
            elif is_a(role, "class"):
                student['class'] = int(role.name[8:13])
            return

def remove_role(id, role, grade=None):
    if role == "current grade":
        for student in students_json:
            # Find the user based on Discord ID
            if student['contacts']['discord'] == id:
                if grade:
                    student['classes'][grade] = []
                return
    elif role == "class":
        for student in students_json:
            # Find the user based on Discord ID
            if student['contacts']['discord'] == id:
                # p.debug("Found student and am going to remove class")
                try:
                    student['class'] = None
                except KeyError:
                    pass
    else:
        for student in students_json:
            # Find the user based on Discord ID
            if student['contacts']['discord'] == id:
                # If it is a subject, remove it from their grade
                if is_a(role, "subject") and grade is not None:
                    try:
                        student['classes'][grade].remove(role.name.upper())
                    except ValueError:
                        print("Database lag lol")
                # If it is their class, remove it from their class
                elif is_a(role, "class"):
                    try:
                        student['class'] = None
                    except KeyError:
                        pass
                return

def sync_students_json():
    with open("JSON-Data/students.json", "w") as fw:
        if json_is_condensed:
            json.dump(students_json, fw, separators=(",", ":"))
        else:
            json.dump(students_json, fw, indent=2)

# Making sure all students added during the running of this script are saved when it ends
# Doesn't work with main.py for whatever reason
def students_exit():
    with open("JSON-Data/students.json", "w") as fw:
        json.dump(students_json, fw, separators=(",", ":"))
atexit.register(students_exit)

if __name__ == "__main__":
    # Adding students to test
    GATLEN = Student(discord="HugernotYT#3805")
    # Student(discord="HugernotYT#3805")
    NIC = Student(discord="copew3ll#5232")
    BAD_NAME = Student()

    # Making sure that the students are parsed to JSON correctly
    student_parsed = json.dumps(students_json)
    print(student_parsed)

from colorama import Fore, Back, Style
import pretty_printing as p
cmd_prefix = "."

class Cmd:
    cmds = []
    # Cmd takes the name of the command, the role permissions, user
    # permissions, a description, and an execution
    def __init__(self,
        execution,
        name="EMPTY NAME",
        #no perms means everyone has access
        role_perms=["all"],
        user_perms=["all"],
        description="EMPTY DESCRIPTION"
        ):

        Cmd.cmds.append(self)

        self.name = name.upper()
        self.role_perms = role_perms
        self.user_perms = user_perms
        self.description = description
        self.execution = execution

        p.startup(self.name, "has been created.")

    async def __call__(self, message, uIn):
        if uIn.name == self.name:
            p.title(" [START " + self.name + "] by user " +
                message.author.name)
            await self.execution(self, message, uIn)
            p.endline(" [END " + self.name + "]")
            return True
        else:
            return False

    @staticmethod
    def parse(str):
        # Parses a command/string into a usable format
        str = str.replace(" ", "").upper()
        args = str.split("[")[1:]
        sub_args = []

        for section in args:
            sub_args.append(section.split(","))

        class Cmd_obj:
            name = str[1:].split("[")[0]
            args = sub_args

            def __str__(self):
                return "Name: " + Cmd_obj.name + "\nArgs: " + Cmd_obj.args
                # Doesn't work
        return Cmd_obj

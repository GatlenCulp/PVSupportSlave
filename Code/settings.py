from command_class import cmd_prefix

# Change PVSG_id, roles_ids

PVSG_id="352526965565161494"
role_ids=[]
log_off_message = "Bot going offline"
gatlen = "195344400912482304"
still_need_class_role_msg = ("To use the bot, you need to have a class role. To get this role, goto PVSG and type `" + cmd_prefix +
    "assign [Class of XXXX` where the Xs are the year you graduate.")
json_is_condensed = False
baseline_role = "352978137325502464"
admin_baseline_role = "352966027916869641"


# Colors setup
color = {"success": 0x33FF33, "error": 0xFF3333, "8": 0x1ABC9C,
    "9": 0x2ECC71, "10": 0x3498DB, "11": 0x9B59B6, "12": 0xE91E63, "class": 0x71368A}

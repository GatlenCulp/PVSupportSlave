# ==============================================================================
# IMPORTS & INITIALIZATION
# ==============================================================================
import discord
import asyncio
import json


cmd_prefix = "."


client = discord.Client()

with open("teachers.json", "r") as f:
    teachers_json = json.loads(f.read())
    print("Read teachers.json\n")
    # print(teachers_json)
    print("Jagdish: " + teachers_json['teachers']['JAGDISH']['fname'])

# ==============================================================================
# FUNCITONS & CLASSES
# ==============================================================================


async def message_all_servers(str):
    for server in client.servers:
        await client.send_message(server.default_channel, str)

def send_message_to_console(message):
    print("\033[36m [MESSAGE] " + message.author.name + ": \"" + message.content +
        "\"\033[0m")

def role_names_to_roles(message, *args):
    # If the name of a role in the server matches the role mentioned, return
    # the role's ID
    print(args)
    role_ids = []
    for role_name in args:
        role_name = role_name.replace(" ", "").upper()
        for role in message.server.roles:
            # print(role_name + " == " + role.name.replace(" ", "").upper()
            #     + "?")
            if role_name == role.name.replace(" ", "").upper():
                role_ids.append(role)
    return role_ids

def p_endline(str):
    # Prints in a special format
    print("\033[1m\033[37\033[41;1m^^^^^^^^^^ {} ^^^^^^^^^^\n\033[0m".format(str.upper()))

def p_debug(str):
    # Prints in a special format to more easily find a printed statement for debugging
    print("\033[7m+++++ {} +++++\n\033[0m".format(str))

def p_title(title):
    # Prints in a special format
    print("\033[1m\033[37m\033[42;1m\n-----===== {} =====-----\033[0m".format(title.upper()))

def find_highest_role(member, server):
    highest = 0
    for role in member.roles:
        if role.position > highest:
            highest = role.position
    print(highest)
    return highest

def if_roles_blow_position(roles, position):
    for role in roles:
        print("-------")
        print(str(role.position) + " " + str(position))
        if role.position > position:
            return False
    return True

async def test_and_run_commands(message):
    if (message.content.startswith(cmd_prefix)):
        print("-----TESTING COMMAND-----")
        uIn = Cmd.parse(message.content)
        # await client.send_message(message.channel, "Name: " + uIn.name +
        # "\nFirst Arg: " + uIn.args[0][0])
        for command in Cmd.cmds:
            print("Testing: " + command.name)
            if await command(message, uIn):
                print("leaving for loop")
                break
            elif command == Cmd.cmds[-1]:
                await client.send_message(message.channel,
                "\"" + message.content + "\" is not a valid command. Use " + cmd_prefix +
                    "help for commands.")

class Cmd:
    cmds = []
    # Cmd takes the name of the command, the role permissions, user
    # permissions, a description, and an execution
    def __init__(self,
        execution,
        name="EMPTY NAME",
        #no perms means everyone has access
        role_perms=[],
        user_perms=[],
        description="EMPTY DESCRIPTION"
        ):

        Cmd.cmds.append(self)

        self.name = name.upper()
        self.role_perms = role_perms
        self.user_perms = user_perms
        self.description = description
        self.execution = execution

        print("\033[33m" + self.name + " has been created.")

    async def __call__(self, message, uIn):
        if uIn.name == self.name:
            p_title(" [START " + self.name + "] by user " +
                message.author.name)
            # await client.send_message(message.channel, self.name + " has been run.")
            await self.execution(self, message, uIn)
            p_endline(" [END " + self.name + "]")
            return True
        else:
            return False

    @staticmethod
    def parse(str):
        # Parses a command/string into a usable format
        str = str.replace(" ", "").upper()
        args = str.split("[")[1:]
        sub_args = []

        for section in args:
            sub_args.append(section.split(","))

        class Cmd_obj:
            name = str[1:].split("[")[0]
            args = sub_args

            def __str__(self):
                return "Name: " + Cmd_obj.name + "\nArgs: " + Cmd_obj.args
                # Doesn't work
        return Cmd_obj

# ==============================================================================
# COMMAND DECLARATIONS
# ==============================================================================

# ==== EMPTY ==================================================================

async def cmd_execution(self, message, uIn):
    await client.send_message(message.channel, "I'm dead inside")

cmdEmpty = Cmd(
    name="EMPTY",
    execution=cmd_execution
    )

# ==== INFO ===================================================================

async def cmd_execution(self, message, uIn):
    # fill out soon
    # print(self.description)
    # await client.send_message(message.channel, self.description)
    if (uIn.args == []):
        await client.send_message(message.channel,
            "Please say the teacher you want info for.")
    elif (len(uIn.args[0]) > 1):
        await client.send_message(message.channel,
            "Enter only one teacher name")
    else:
        teacher = uIn.args[0][0]
        try:
            teacher_info = teachers_json['teachers'][teacher]
            await client.send_message(message.channel,
                "{fname} {lname}\n"
                "Website: {web}\n"
                "Roles: {roles}\n"
                "\n"
                "For more info, visit: {info_page}".format(
                    fname=teacher_info['fname'].title(),
                    lname=teacher.title(),
                    web=teacher_info['web'],
                    roles=", ".join(teacher_info['roles']).title(),
                    info_page="http://sarasotacountyschools.net/schools/"
                    "pineview/teacher-directory.aspx?id=" + teacher_info['id']
                )
            )
        except KeyError:
            await client.send_message(message.channel,
                "Teacher not found")

cmdInfo = Cmd(
    name="info",
    description= "**" + cmd_prefix + "info [teacher:** Prints out teacher info "
    "as \n"
    "```fname lname (title case):\n"
    "Website: web url, say no website found\n"
    "Roles: list roles here\n"
    "\n"
    "For more info visit: The url for PV: Contact info```",
    execution=cmd_execution
    )

# ==== HELP ===================================================================

def format_help_for_command(command):
    return ("**{name}**: {description}\nExecutable by {role_perms} "
    "role(s) and {user_perms} user(s)\n\n".format(
        name=command.name,
        description=command.description,
        role_perms=command.role_perms,
        # roles_id_to_name(*command.role_perms),
        # roles_id_to_name(*["433398100619558932"])
        user_perms=command.user_perms
    ))

async def cmd_execution(self, message, uIn):
    output = ""
    if uIn.args == []:
        for command in Cmd.cmds:
            output += format_help_for_command(command)
    elif len(uIn.args) > 1:
        await client.send_message(message.channel, "Please, input only one"
        "argument set")
    else:
        for input_cmd in uIn.args[0]:
            for command in Cmd.cmds:
                if input_cmd == command.name:
                    output += format_help_for_command(command)
    await client.send_message(message.channel, output)

cmdHelp = Cmd(
    name="HELP",
    description="**.help [+command:** Lists all the commands, "
    "their descriptions, arguments/options, and who can access them. "
     "When the \"command\" argument is "
     "included, it will instead give a larger description of what the "
     "command does.",
    execution=cmd_execution
    )

# ==== ASSIGN =================================================================

async def cmd_execution(self, message, uIn):
    # for role in message.channel.server.roles:
    #     print(role.name + " " + str(role.position))
    if uIn.args == []:
        await client.send_message(message.channel, "Please enter the roles you"
            " wish to add.")
    else:
        roles_to_add = role_names_to_roles(message, *uIn.args[0])
        print(roles_to_add)
        if (if_roles_blow_position(roles_to_add, find_highest_role(message.author,
                message.channel.server))):
            await client.send_message(message.channel, "Your current permissions"
            " don't allow you to assign that role or the role doesn't exist.")
        #the role they want is above their permissions, tell them they can't add it
        else:
            await client.add_roles(message.author, *roles_to_add)

cmdAssign = Cmd(
    name="ASSIGN",
    description="**.assign [role, role, role...:** Assigns the roles passed in"
    " as arguments",
    execution=cmd_execution
    )

# ==============================================================================

del cmd_execution
# ==============================================================================
# EVENTS
# ==============================================================================


@client.event
async def on_ready():
    p_title("Bot Ready")
    # await message_all_servers("Bot online and ready to terminate")


@client.event
async def on_message(message):
    send_message_to_console(message)
    await test_and_run_commands(message)


# ==============================================================================
# RUNNING SCRIPT
# ==============================================================================

# Storing private token in another file that isn't uploaded
token = open("token.txt", "r").readline().strip("\n")
client.run(token)

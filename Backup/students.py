import atexit, json, logging, uuid

# Json of all the students
with open("students.json", "r") as f:
    students_json = json.load(f)

def Student(discord=None, phone=None, email=None):
    # Weird ass dictionary pseudoclass that creates a student for JSON database
    # Ensure these arguments aren't empty
    if any((discord, phone, email)):
        # See if they are already in the system first
        for student in students_json:
            if (student['contacts']['discord'] == discord and
                student['contacts']['phone'] == phone and
                student['contacts']['email'] == email):
                print("DUPLICATE ALERT! DUPLICATE ALERT FOR", discord)
                return
        # If they aren't in the system, add them
        # Create their contacts
        contacts = {'discord': discord, 'phone': phone, 'email': email}
        # Add them to the database
        output = {
            "uid": str(uuid.uuid4()),
            "contacts": contacts,
            "classes": {
                "8": [],
                "9": [],
                "10": [],
                "11": [],
                "12": []
            },
            "class": None
        }
        students_json.append(output)
        return output
    # The args were empty, so print an error (one arg is needed)
    else:
        print("ERROR: You must include a discord, phone or email when initializing a student")

# Making sure all students added during the running of this script are saved when it ends
# Doesn't work with main.py for whatever reason
def students_exit():
    with open("students.json", "w") as fw:
        json.dump(students_json, fw, indent=2)
atexit.register(students_exit)

if __name__ == "__main__":
    # Adding students to test
    GATLEN = Student(discord="HugernotYT#3805")
    # Student(discord="HugernotYT#3805")
    NIC = Student(discord="copew3ll#5232")
    BAD_NAME = Student()

    # Making sure that the students are parsed to JSON correctly
    student_parsed = json.dumps(students_json)
    print(student_parsed)

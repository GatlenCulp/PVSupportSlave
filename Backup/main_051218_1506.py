# ==============================================================================
# IMPORTS & INITIALIZATION
# ==============================================================================
import discord
import asyncio
import json
import pretty_printing as p
from command_class import Cmd, cmd_prefix

client = discord.Client()

with open("teachers.json", "r") as f:
    teachers_json = json.loads(f.read())
    # print("Read teachers.json\n")
    # print(teachers_json)
    # print("Jagdish: " + teachers_json['teachers']['JAGDISH']['fname'])

# ==============================================================================
# FUNCITONS & CLASSES
# ==============================================================================

async def message_all_servers(str):
    for server in client.servers:
        await client.send_message(server.default_channel, str)

def send_message_to_console(message):
    print("\033[36m [MESSAGE] " + message.author.name + ": \"" + message.content +
        "\"\033[0m")

def role_names_to_roles(server, *args):
    # If the name of a role in the server matches the role mentioned, return
    # the role's ID
    # p.debug(args)
    role_ids = []
    for role_name in args:
        role_name = role_name.replace(" ", "").upper()
        for role in server.roles:
            # print(role_name + " == " + role.name.replace(" ", "").upper()
            #     + "?")
            if role_name == role.name.replace(" ", "").upper():
                role_ids.append(role)
    return role_ids

def find_highest_role(member, server):
    highest = 0
    for role in member.roles:
        if role.position > highest:
            highest = role.position
    # p.debug(highest)
    return highest

def if_roles_below_position(roles, position):
    if roles == []:
        return False
    for role in roles:
        # p.debug("Role asked for: ", role.position, "Highest role pos: ",
        # position, " and if ", role.position, " > ", position, " then returns false")
        if role.position > position:
            return False
    return True

async def test_and_run_commands(message):
    if (message.content.startswith(cmd_prefix)):
        # print("-----TESTING COMMAND-----")
        uIn = Cmd.parse(message.content)
        # await client.send_message(message.channel, "Name: " + uIn.name +
        # "\nFirst Arg: " + uIn.args[0][0])
        for command in Cmd.cmds:
            # print("Testing: " + command.name)
            if await command(message, uIn):
                # print("leaving for loop")
                break
            elif command == Cmd.cmds[-1]:
                await client.send_message(message.channel,
                "\"" + message.content + "\" is not a valid command. Use " + cmd_prefix +
                    "help for commands.")

# ==============================================================================
# COMMAND DECLARATIONS
# ==============================================================================

# ==== EMPTY ==================================================================

async def cmd_execution(self, message, uIn):
    await client.send_message(message.channel, "I'm dead inside")

cmdEmpty = Cmd(
    name="EMPTY",
    execution=cmd_execution
    )

# ==== INFO ===================================================================

async def cmd_execution(self, message, uIn):
    # fill out soon
    # print(self.description)
    # await client.send_message(message.channel, self.description)
    if (uIn.args == []):
        await client.send_message(message.channel,
            "Please say the teacher you want info for.")
    elif (len(uIn.args[0]) > 1):
        await client.send_message(message.channel,
            "Enter only one teacher name")
    else:
        teacher = uIn.args[0][0]
        try:
            teacher_info = teachers_json['teachers'][teacher]
            await client.send_message(message.channel,
                "{fname} {lname}\n"
                "Website: {web}\n"
                "Roles: {roles}\n"
                "\n"
                "For more info, visit: {info_page}".format(
                    fname=teacher_info['fname'].title(),
                    lname=teacher.title(),
                    web=teacher_info['web'],
                    roles=", ".join(teacher_info['roles']).title(),
                    info_page="http://sarasotacountyschools.net/schools/"
                    "pineview/teacher-directory.aspx?id=" + teacher_info['id']
                )
            )
        except KeyError:
            await client.send_message(message.channel,
                "Teacher not found")

cmdInfo = Cmd(
    name="info",
    description= "**" + cmd_prefix + "info [teacher:** Prints out teacher info "
    "as \n"
    "```fname lname (title case):\n"
    "Website: web url, say no website found\n"
    "Roles: list roles here\n"
    "\n"
    "For more info visit: The url for PV: Contact info```",
    execution=cmd_execution
    )

# ==== HELP ===================================================================

def format_help_for_command(command):
    return ("**{name}**: {description}\nExecutable by {role_perms} "
    "role(s) and {user_perms} user(s)\n\n".format(
        name=command.name,
        description=command.description,
        role_perms=command.role_perms,
        # roles_id_to_name(*command.role_perms),
        # roles_id_to_name(*["433398100619558932"])
        user_perms=command.user_perms
    ))

async def cmd_execution(self, message, uIn):
    output = ""
    if uIn.args == []:
        for command in Cmd.cmds:
            output += format_help_for_command(command)
    elif len(uIn.args) > 1:
        await client.send_message(message.channel, "Please, input only one"
        "argument set")
    else:
        for input_cmd in uIn.args[0]:
            for command in Cmd.cmds:
                if input_cmd == command.name:
                    output += format_help_for_command(command)
    await client.send_message(message.channel, output)

cmdHelp = Cmd(
    name="HELP",
    description="**.help [+command:** Lists all the commands, "
    "their descriptions, arguments/options, and who can access them. "
     "When the \"command\" argument is "
     "included, it will instead give a larger description of what the "
     "command does.",
    execution=cmd_execution
    )

# ==== ASSIGN =================================================================

async def cmd_execution(self, message, uIn):
    # for role in message.channel.server.roles:
    #     print(role.name + " " + str(role.position))
    if uIn.args == []:
        await client.send_message(message.channel, "Please enter the roles you"
            " wish to add.")
    else:
        roles_to_add = role_names_to_roles(message.server, *uIn.args[0])
        # p.debug("Checking that names have been converted to roles: ", roles_to_add)
        if (if_roles_below_position(roles_to_add, find_highest_role(message.author,
                message.channel.server))):
                await client.add_roles(message.author, *roles_to_add)
                print("Assignment successful")
        # The role they want is above their permissions, tell them they can't add it
        else:
            await client.send_message(message.channel, "Your current permissions"
                " don't allow you to assign that role or the role doesn't exist.")
            print("The user isn't allowed to use the role mentioned or it "
                "doesn't exist")

cmdAssign = Cmd(
    name="ASSIGN",
    description="**.assign [role, role, role...:** Assigns the roles passed in"
    " as arguments",
    execution=cmd_execution
    )

# ==== DELETION ================================================================

async def cmd_execution(self, message, uIn):
    role_on_join = "433398100619558932"

    member = message.author
    print("command ran")
    await client.add_roles(message.author, role_on_join)

cmdJoin = Cmd(
    name="JOIN",
    description="**.join:** Development command. Used to simulate what would "
    "happen if the person who runs it were to join the server",
    execution=cmd_execution
)

# ==== DELETION ================================================================

del cmd_execution
# ==============================================================================
# EVENTS
# ==============================================================================


@client.event
async def on_ready():
    p.title("Bot Ready")
    # await message_all_servers("Bot online and ready to terminate")

# @client.event
# async def on_member_join(member):
#     p.debug(member.name)

@client.event
async def on_message(message):
    send_message_to_console(message)
    await test_and_run_commands(message)


# ==============================================================================
# RUNNING SCRIPT
# ==============================================================================

# Storing private token in another file that isn't uploaded
token = open("token.txt", "r").readline().strip("\n")
client.run(token)

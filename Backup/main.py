# http://discordpy.readthedocs.io/en/latest/api.html
# ERROR ON ADDING JUNIOR ROLE 1 TO ME

# ==============================================================================
# IMPORTS & INITIALIZATION
# ==============================================================================
import asyncio, atexit, datetime, discord, json, logging, math, os, sys, threading
import pretty_printing as p
from students import students_json, Student
from colorama import Fore, Back, Style
from command_class import Cmd, cmd_prefix

PVSG_id="304068885144141825"
role_ids=["433398100619558932"]
logger_enabled = False
log_off_message = "Bot going offline"
gatlen = "195344400912482304"
still_need_class_role_msg = ("To use the bot, you need to have a class role. To get this role, goto PVSG and type `" + cmd_prefix +
    "assign [Class of XXXX` where the Xs are the year you graduate.")

# Colors setup
color = {"success": 0x33FF33, "error": 0xFF3333, "8": 0x1ABC9C,
    "9": 0x2ECC71, "10": 0x3498DB, "11": 0x9B59B6, "12": 0xE91E63, "class": 0x71368A}

# Creating root logger and script logger
if logger_enabled:
    root_log_level = logging.INFO
    # Making a folder with the date as the name
    time_ran = str(datetime.datetime.now())
    log_folder = "./logs/{time_ran} (running)".format(time_ran=time_ran)
    os.makedirs(log_folder)
    # Setting up root log
    logging.basicConfig(filename=log_folder + "/root.log",
                        format="%(name)s: %(filename)s on %(lineno)d @ %(asctime)s\
                        \n\t%(levelname)s: %(message)s",
                        level=root_log_level)
    # Other stuff
    logging.info("The log level is set at {level}".format(level=root_log_level))
    logger = logging.getLogger(__name__)
    print(Fore.YELLOW, "Logger created and configured", Style.RESET_ALL)

# Discord client open
client = discord.Client() #loop=async_loop
print(Fore.YELLOW, "Discord client created", Style.RESET_ALL)

# Open teachers.json
with open("teachers.json", "r") as f:
    teachers_json = json.load(f)
print(Fore.YELLOW, "teachers.json imported", Style.RESET_ALL)

# Open links.json
with open("links.json", "r") as f:
    links_json = json.load(f)
print(Fore.YELLOW, "links.json imported\033[0m", Style.RESET_ALL)

# Save links.json when system closes
def exit_function():
    with open("links.json", "w") as fw:
        json.dump(links_json, fw, indent=2)
    # if logger_enabled:
    #     exit_time = datetime.datetime.now()
    #     # This try/except doesn't work
    #     try:
    #         # Renaming folder to contain the start and end time of the script
    #         os.rename(log_folder,
    #                     "./logs/{time_ran} to {exit_time}".format(time_ran=time_ran, exit_time=exit_time))
    #     except FileNotFoundError:
    #         pass

atexit.register(exit_function)
# ==============================================================================
# FUNCITONS & CLASSES
# ==============================================================================

async def message_all_servers(str=None, embed=None):
    if embed == None:
        for server in client.servers:
            await client.send_message(server.default_channel, str)
    else:
        for server in client.servers:
            await client.send_message(server.default_channel, embed=embed)

def send_message_to_console(message):
    if message.server == None:
        print("\033[36m [MESSAGE] ", message.channel, " > ",
        message.author.name, ": \"",
        message.content +"\"\033[0m")
    else:
        print("\033[36m [MESSAGE] ", message.server.name, " > ",
        message.channel.name, " > ", message.author.name, ": \"",
        message.content +"\"\033[0m")

def role_names_to_roles(server, *args):
    # If the name of a role in the server matches the role mentioned, return
    # the role
    roles_converted = []
    for role_name in args:
        role_name = role_name.replace(" ", "").upper()
        for role in server.roles:
            if role_name == role.name.replace(" ", "").upper():
                roles_converted.append(role)
    return roles_converted

def role_ids_to_roles(server, *role_ids):
    # If the ID of a role in the server matches the role mentioned, return
    # the role
    roles_converted = []
    for role_id in role_ids:
        for role in server.roles:
            if role_id == role.id:
                roles_converted.append(role)
    return roles_converted

def if_roles_below_position(roles, pos):
    if roles == []:
        return False
    for role in roles:
        # p.debug(role.position, " < ", pos.position, "? ", (role.position < pos.position))
        # If the role is above the position, then say FALSE, the roles are above the position
        if role.position > (pos.position-1):
            return False
    return True

async def join(member):
    welcome_title = "**Welcome to PVSG!**"
    timeout = 5 * 60
    p.debug(member)

    # Add the roles on join
    roles_on_join = role_ids_to_roles(PVSG, *role_ids)
    await client.add_roles(member, *roles_on_join)
    # Send them their first welcome message
    em = discord.Embed(
        title=welcome_title,
        description="Hello there, and welcome to the PineView Support Group (PVSG)! I'm the "
        "PVSupportSlave, written and "
        "maintained by Gatlen Culp (Hugernot#3805) if you need any help, just talk "
        "to him. Now tell me, are you new to this app? (Respond with yes/no or y/n)",
        color=color['success']
        )
    await client.send_message(member, embed=em)
    # Add the user to the database
    Student(discord=member.id)

    # Wait for user to respond whether or not they are new
    def check(message):
        if message.content.upper().startswith("Y") or message.content.upper().startswith("N"):
            return True
        else:
            return False
    answer = await client.wait_for_message(
        timeout=timeout,
        author=member,
        check=check
        )
    if answer == None:
        em = discord.Embed(
            title=welcome_title,
            description="You didn't respond soon enough. I had a speech and all planned for you :(. **Sigh** It's okay though... I'm used to it. No one takes a bot seriously... If you want to learn how the server works and all just talk to the admins because apparently I'm useless.",
            color=color['error']
            )
        await client.send_message(member,
            "You didn't respond in time you silly goose. Talk to an admin to "
            "get your roles.")
    else:
        answer = answer.content.upper()
        # Yes, they are new. Send them these messages.
        if answer.startswith("Y"):
            em = discord.Embed(
                    title=welcome_title,
                    description=
                        ("Alrighty, let me give you a quick rundown about how to use Discord, if you don't feel like reading this like the majority of the people, follow this here link:\n\n"

                        "**Desktop:** https://youtu.be/E7xznRGg9WM\n\n"

                        "**Mobile:** https://youtu.be/vKokUvv81cM (Tutorial isn't as good, goes into more depth than you need to know, but it's the best one I can find, so I'd just use the one above)\n\n"

                        "One thing I'm going to ask you to do is change Discord to Dark mode instead of Light mode because you are most likely going to get blinded- especially if you use your electronics during the night.\n\n"

                        "**Go to User settings** (bottom left, for mobile you have to tap the menu in the top right before the settings icon appears in the bottom left) **=> Appearance => Theme => Dark**\n\n"

                        "I'm going to ask one thing of you, *give the PineView Support Group (PVSG) a chance*. This server/chat has been around a long time and has helped us countless times. The more people in the chat, the more efficient it is as helping people. Try to use it at least for a little bit and if you really don't want to use it after that, there's no one holding you hostage. Except for maybe me. Call your parents now and tell them you aren't coming home tonight. ;)"
                        ),
                    color=color['success']
                )
            await client.send_message(member, embed=em)
            em = discord.Embed(
                    title=welcome_title,
                    description=
                        (
                        "So on Discord you are able to **mention** someone or a group of people by doing **@name**. What this does is that it will notify the person you mention that you mentioned them in a message and then they can respond. This is so that your phone doesn't blow up and they only get notifications when you need them.\n\n"

                        "Example of a mention: @PVSupportSlave#6612\n\n"

                        "The top icon on the bar on your left is your **direct messages**. This is where you can create group chats or just chat with an individual person. Everything else below it is something called a **server**, kind of like a massive, more organized group chat. A server is split into channels for different things such as **text channels** for announcements or texting. Then there are **audio channels** in which you can connect to and talk to anyone else in that channel. On the server you can tag people with these things called **roles** which can give certain permissions (such as the ability to change nicknames or change the server in some way) or just be a nice indicator."
                        ),
                    color=color['success']
                )
            await client.send_message(member, embed=em)
        # Nah, these beans had Discord. Send them these messages.
        else:
            em = discord.Embed(
                    title=welcome_title,
                    description=
                        "Well dang son/daughter/genderless person/apache helicopter, I'm proud of you. Brings a tear to my eye knowing that you use Discord :sleepy:.",
                    color=color['success']
                )
            await client.send_message(member, embed=em)
        # And hey, send them this message regardless explaining the server
        em = discord.Embed(
                title=welcome_title,
                description=
                    (
                    "*So, on the PV Support Group server we use roles to tag people with what classes they have.* Why would we do this? Because you can mention everyone with a role by doing **@RoleName**\n\n"

                    "Example (it wont work in DMs bc there aren't any roles here): @algebra II – robertson.\n\n"

                    "And then you can add whatever question you have for everyone with that class so that they will get a notification \"@algebra II - robertson What was the homework?\"\n\n"

                    "**I'm going to need your classes, clubs, and full name for the server. That way you'll be notified whenever someone has a question. Again, if you need help, contact Hugernot#3805 (Gatlen).**\n\n"

                    "The syntax for all of the class roles are `[subject] - [teacher]`. Try assigning you roles now in PVSG by using `.assign [ROLE_NAME_HERE`. For example, if you have Robertson for math, use `.assign [algebra II - robertson`. If you are unsure what the right role name is, don't worry about it, just ask someone for help. NOTE THAT YOU CAN'T DO ROLE ASSIGNMENTS HERE. You can only do them in the PVSG server because of how I work.\n\n"

                    "I hope you enjoy the server! PLEASE STAY AND GIVE IT CHANCE!"
                    ),
                color=color['success']
            )
        await client.send_message(member, embed=em)


        # Wait for them to give roles
        # answer = await client.wait_for_message(
        #     timeout=timeout,
        #     author=member,
        #     check=check
        #     )
        # And if it times out, then say they didn't respond in time
        # if answer == None:
        #     em = discord.Embed(
        #         title=welcome_title,
        #         description="You didn't respond in time you silly goose. Talk to an admin to "
        #             "get your roles.",
        #         color=color['error']
        #     )
        #     await client.send_message(member, embed=em)

def find_different_item(list1, list2):
    set1 = set(list1)
    set2 = set(list2)
    longest_set = max(set1, set2, key=len)
    shortest_set = min(set1, set2, key=len)
    for item in longest_set:
        if item in shortest_set:
            continue
        else:
            return item

def is_a(role, type):
    role_color = role.color.value
    if type == "subject":
        return (role_color == color['8'] or role_color == color['9'] or role_color == color['10'] or
        role_color == color['11'] or role_color == color['12'])
    else:
        # Used for detecting if a role is a certain class role, club role, subject role, etc
        try:
            return role_color == color[type]
        except KeyError:
            return

def get_emoji(name):
    while True:
        try:
            emoji = next(emojis)
            if emoji.name == name:
                return emoji
        except StopIteration:
            p.debug("Emoji not found")
            return

async def test_and_run_commands(message):
    if (message.content.startswith(cmd_prefix)):
        # Parse the message content
        uIn = Cmd.parse(message.content)
        # Test the list of commands, run and break if found
        for command in Cmd.cmds:
            found = await command(message, uIn)
            if found:
                return
        # If the command isn't found, message the user that is so
        em = discord.Embed(
            title="ERROR",
            description="\"" + message.content + "\" is not a valid command. Use " + cmd_prefix +
                "help for commands.",
            color=color['error']
        )
        await client.send_message(message.channel, embed=em)

async def send_pages(channel, member, self=None, message="message", color=color['success']):
    '''It will take the message given, and if it is above Discord's character limit,
    it will turn it into a page-flipping messsage where if you click the arrows (reactions)
    on the bottom of the message, it will be edited to be the next page'''
    # Just an example of the page flipping string
    # message = ""
    # for i in range(2000):
    #     message += str(i) + " "
    # 1. Detect if the message is over character limit
    if len(message) < 2000:
        if self == None:
            em = discord.Embed(title="Pages", description=message, color=color)
        else:
            em = discord.Embed(title=self.name, description=message, color=color)
        await client.send_message(channel, embed=em)
    # 2. Otherwise split it at the character limit, make it into an array of embedded
    # messages of 2000 characters
    else:
        pages = []
        current_page_num = 0
        for i in range(math.ceil(len(message)/2000)):
            pages.append(message[2000*i:2000*(i+1)])
        del message
        # 3. Display first message
        if self == None:
            em = discord.Embed(title="Page " + str(current_page_num + 1) + "/" + str(len(pages)), description=pages[0], color=color)
        else:
            em = discord.Embed(title=self.name + " Page " + str(current_page_num + 1) + "/" + str(len(pages)), description=pages[0], color=color)
        msg_obj = await client.send_message(channel, embed=em)
        # 4. Add reactions/arrows to go back and forth
        while True:
            await client.clear_reactions(msg_obj)
            await client.add_reaction(msg_obj, "⬅")
            await client.add_reaction(msg_obj, "➡")
            # 5. Await a reaction/page flipping
            reaction = await client.wait_for_reaction(emoji=["⬅", "➡"], user=member, timeout=5*60, message=msg_obj)
            # Message timed out
            if reaction == None:
                break
            # Flipped forward
            elif reaction.reaction.emoji == "⬅":
                current_page_num -= 1
                if current_page_num < 0:
                    current_page_num = len(pages) - 1
            # Flipped backward
            elif reaction.reaction.emoji == "➡":
                current_page_num += 1
                if current_page_num > (len(pages) - 1):
                    current_page_num = 0
            # Send the current page
            if self == None:
                em = discord.Embed(title="Page " + str(current_page_num + 1) + "/" + str(len(pages)), description=pages[current_page_num], color=color)
            else:
                em = discord.Embed(title=self.name + " Page " + str(current_page_num + 1) + "/" + str(len(pages)), description=pages[current_page_num], color=color)
            await client.edit_message(msg_obj, embed=em)

        # 6. Edit or delete previous message to make room for the next message

def has_class_role(aft):
        # Make sure they have a class role, if not, don't do this. Wait for the day to be over for them to get
        # Pinged about the role
        for role in aft.roles:
            # Okay, they pass
            if is_a(role, 'class'):
                return role
            # NAY, LEAVE
            elif role == aft.roles[-1]:
                return False

def class_to_grade(class_role):
    # 12 - (class_of - year)
    if class_role:
        if isinstance(class_role, int) is not True:
            class_year = int(class_role.name[8:13])
        else:
            class_year = class_role
        grad_date = datetime.date(class_year, 6, 1)
        today = datetime.date.today()
        time_til_grad = grad_date - today
        grade = str(12 - math.floor((time_til_grad.days/365)))
        return grade
    else:
        return None

def index_plus(list, item):
    try:
        return list.index(item)
    except ValueError:
        return -1

def add_role(id, role, grade=None):
    for student in students_json:
        # Find the user based on Discord ID
        if student['contacts']['discord'] == id:
            # If it is a subject, add it to their grade
            if is_a(role, "subject"):
                name = role.name.upper()
                # Make sure there are no duplicates and grade is given
                if index_plus(student['classes'][grade], name) is -1 and grade != None:
                    student['classes'][grade].append(name)
            # If it is their class, add it to their class
            elif is_a(role, "class"):
                student['class'] = int(role.name[8:13])
            return

def remove_role(id, role, grade=None):
    if role == "current grade":
        for student in students_json:
            # Find the user based on Discord ID
            if student['contacts']['discord'] == id:
                if grade:
                    student['classes'][grade] = []
                return
    elif role == "class":
        for student in students_json:
            # Find the user based on Discord ID
            if student['contacts']['discord'] == id:
                # p.debug("Found student and am going to remove class")
                try:
                    student['class'] = None
                except KeyError:
                    pass
    else:
        for student in students_json:
            # Find the user based on Discord ID
            if student['contacts']['discord'] == id:
                # If it is a subject, remove it from their grade
                if is_a(role, "subject") and grade is not None:
                    student['classes'][grade].remove(role.name.upper())
                # If it is their class, remove it from their class
                elif is_a(role, "class"):
                    try:
                        student['class'] = None
                    except KeyError:
                        pass
                return

async def sync_roles(PVSG=None, student=None):
    p.debug("SYNCING NIBBA")
    if PVSG != None:
        for member in PVSG.members:
            if member == client.user:
                continue
            for student in students_json:
                # Student found in database
                if member.id == student['contacts']['discord']:
                    # p.debug(member.name, " was found")
                    # Update the class role
                    for role in member.roles:
                        # There is a class role, sync it
                        if is_a(role, "class"):
                            # p.debug("Yes class role")
                            add_role(member.id, role)
                            break
                        # There isn't a class role, delete it
                        elif role == member.roles[-1]:
                            # p.debug("No class role")
                            remove_role(member.id, "class")
                            break
                    grade = class_to_grade(student['class'])
                    # Tell the student they still need a class role
                    # p.debug("Grade: ", grade)
                    if grade is None:
                        pass
                        # try:
                        #     await send_pages(channel=member, member=member, message=still_need_class_role_msg, color=color['success'])
                        # except discord.errors.Forbidden:
                        #     print("The user ", member.name, " has the bot blocked most likely")
                    else:
                        remove_role(member.id, "current grade", grade)
                        for role in member.roles:
                            add_role(member.id, role, grade)
                    break
                # The student isn't in the database yet, add them
                elif student == students_json[-1]:
                    await join(member)
    with open("students.json", "w") as fw:
        json.dump(students_json, fw, indent=2)

async def msg_members_without_class_role():
    p.debug("Messaging students who don't have class role")
    # find student, check their class attribute, if None, send message
    for student in students_json:
        for member in PVSG.members:
            if student['contacts']['discord'] == member.id:
                if student['class'] == None:
                    await send_pages(member, member, message=still_need_class_role_msg)
                break

async def async_loop():
    # Timed loop for discord
    sync_rate = (60 * 2) # seconds
    sync_timer = 0 # seconds

    need_class_role_rate = (60 * 60 * 24) # seconds
    need_class_role_timer = 0 # seconds

    while True:
        await asyncio.sleep(1)
        sync_timer += 1
        need_class_role_timer += 1

        if sync_timer >= (sync_rate):
            sync_timer = 0
            await sync_roles(PVSG)

        if need_class_role_timer >= (need_class_role_rate):
            need_class_role_timer = 0
            await msg_members_without_class_role()

async def NUKE():
    for member in PVSG.members:
        for role in member.roles:
            # print("Member: ", member.name, " Role: ", role.name)
            if is_a(role, "subject"):
                await client.remove_roles(member, role)
# ==============================================================================
# COMMAND DECLARATIONS
# ==============================================================================

# ==== EMPTY ==================================================================

async def cmd_execution(self, message, uIn):
    em = discord.Embed(title=self.name, description="I'm dead inside", color=color['success'])
    await client.send_message(message.channel, embed=em)

cmdEmpty = Cmd(
    name="EMPTY",
    execution=cmd_execution
    )

# ==== INFO ===================================================================

async def cmd_execution(self, message, uIn):
    # fill out soon
    if (uIn.args == []):
        em = discord.Embed(title=self.name, description="Please say the teacher you want info for.", color=color['error'])
        await client.send_message(message.channel, embed=em)
    elif (len(uIn.args[0]) > 1):
        em = discord.Embed(title=self.name, description="Enter only one teacher name", color=color['error'])
        await client.send_message(message.channel, embed=em)
    else:
        teacher = uIn.args[0][0]
        try:
            teacher_info = teachers_json['teachers'][teacher]
            em = discord.Embed(title=self.name, description=(
                "{fname} {lname}\n"
                "Website: {web}\n"
                "Roles: {roles}\n"
                "\n"
                "For more info, visit: {info_page}".format(
                    fname=teacher_info['fname'].title(),
                    lname=teacher.title(),
                    web=teacher_info['web'],
                    roles=", ".join(teacher_info['roles']).title(),
                    info_page="http://sarasotacountyschools.net/schools/"
                    "pineview/teacher-directory.aspx?id=" + teacher_info['id']
                    )
                ), color=color['success'])
            await client.send_message(message.channel, embed=em)
            logging.info(message.author.name + " used INFO to get the info of " + teacher)
        except KeyError:
            em = discord.Embed(title=self.name, description="Teacher not found", color=color['error'])
            await client.send_message(message.channel, embed=em)

cmdInfo = Cmd(
    name="info",
    description= "**" + cmd_prefix + "info [teacher:** Prints out teacher info "
    "as \n"
    "```fname lname (title case):\n"
    "Website: web url, say no website found\n"
    "Roles: list roles here\n"
    "\n"
    "For more info visit: The url for PV: Contact info```",
    execution=cmd_execution
    )

# ==== HELP ===================================================================

def format_help_for_command(command):
    return ("**{name}**: {description}\nExecutable by {role_perms} "
    "role(s) and {user_perms} user(s)\n\n".format(
        name=command.name,
        description=command.description,
        role_perms=command.role_perms,
        # roles_id_to_name(*command.role_perms),
        # roles_id_to_name(*["433398100619558932"])
        user_perms=command.user_perms
    ))

async def cmd_execution(self, message, uIn):
    output = ""
    if uIn.args == []:
        for command in Cmd.cmds:
            output += format_help_for_command(command)
    elif len(uIn.args) > 1:
        em = discord.Embed(title=self.name, description="Please input only one argument set.", color=color['error'])
        await client.send_message(message.channel, embed=em)
        return
    else:
        for input_cmd in uIn.args[0]:
            for command in Cmd.cmds:
                if input_cmd == command.name:
                    output += format_help_for_command(command)
    await send_pages(message.channel, member=message.author, self=self, message=output, color=color['success'])

cmdHelp = Cmd(
    name="HELP",
    description="**.help [+command:** Lists all the commands, "
    "their descriptions, arguments/options, and who can access them. "
     "When the \"command\" argument is "
     "included, it will instead give a larger description of what the "
     "command does.",
    execution=cmd_execution
    )

# ==== ASSIGN =================================================================

baseline_role = "458315096217747467"

async def cmd_execution(self, message, uIn):
    # If there are no arguments...
    if uIn.args == []:
        em = discord.Embed(title=self.name, description="Please enter the roles you"
            " wish to add.", color=color['error'])
        await client.send_message(message.channel, embed=em)
    else:
        # Convert role names to roles
        roles_to_add = role_names_to_roles(message.server, *uIn.args[0])
        # If the roles requested are below their highest role OR the baseline role everyone can take from below
        # p.debug((if_roles_below_position(roles_to_add, message.author.top_role) or
        # if_roles_below_position(roles_to_add, *role_ids_to_roles(message.server, baseline_role))))
        if (if_roles_below_position(roles_to_add, message.author.top_role) or
            if_roles_below_position(roles_to_add,
                *role_ids_to_roles(message.server, baseline_role)
            )
        ):
                await client.add_roles(message.author, *roles_to_add)
                print("Assignment successful")
                em = discord.Embed(title=self.name,
                    description="Role(s) successfully assigned", color=color['success'])
                await client.send_message(message.channel, embed=em)
                logging.info(message.author.name + " used ASSIGN to get the role(s) " + ", ".join(uIn.args[0]))
        # User isn't allowed to assign the roles or the role doesn't exist
        else:
            em = discord.Embed(title=self.name, description="Your current permissions"
                " don't allow you to assign that role or the role doesn't exist.", color=color['error'])
            await client.send_message(message.channel, embed=em)
            print("The user isn't allowed to use the role mentioned or it "
                "doesn't exist")
            logging.warning(message.author.name + " used ASSIGN to get the role(s) " +
                ", ".join(uIn.args[0]) + " but they didn't exist or the user didn't have the permissions")

cmdAssign = Cmd(
    name="ASSIGN",
    description="**.assign [role, role, role... [person/target (the person/target will be a @mention):** Assigns the roles passed in"
    " as arguments. The second parameter is only available to admins and when it is not given, it applies to yourself.",
    execution=cmd_execution
    )

# ==== UNASSIGN =================================================================

async def cmd_execution(self, message, uIn):
    # If there are no arguments...
    if uIn.args == []:
        em = discord.Embed(title=self.name, description="Please enter the roles you"
            " wish to remove.", color=color['error'])
        await client.send_message(message.channel, embed=em)
    else:
        roles_to_remove = role_names_to_roles(message.server, *uIn.args[0])
        p.debug(roles_to_remove)
        # The roles were found on the server
        if roles_to_remove != []:
            await client.remove_roles(message.author, *roles_to_remove)
            print("Unassignment successful")
            em = discord.Embed(title=self.name,
                description="Role(s) successfully unassigned", color=color['success'])
            await client.send_message(message.channel, embed=em)
            logging.info(message.author.name + " used UNASSIGN to REMOVE the role(s) " + ", ".join(uIn.args[0]))
        # The roles were not found on the server
        else:
            em = discord.Embed(title=self.name,
                description="Woah there buddy boy, I don't think that/those was/were legitimate roles", color=color['error'])
            await client.send_message(message.channel, embed=em)

cmdUnassign = Cmd(
    name="UNASSIGN",
    description="**.unassign [role, role, role... [person/target (the person/target will be a @mention):** Unassigns the roles passed in"
    " as arguments. 'class' and 'club' as roles will remove all classes and clubs. The second parameter is only available to admins and when it is not given, it applies to yourself.",
    execution=cmd_execution
    )

# ==== JOIN ====================================================================

async def cmd_execution(self, message, uIn):
    em = discord.Embed(title=self.name,
        description="You should now be recieving a message", color=color['success'])
    await client.send_message(message.channel, embed=em)
    await join(message.author)

cmdJoin = Cmd(
    name="JOIN",
    description="**.join:** Development command. Used to simulate what would "
    "happen if the person who runs it were to join the server",
    execution=cmd_execution
    )

# ==== STOP ====================================================================

admin_baseline_role = "458315096217747467"

async def cmd_execution(self, message, uIn):
    if message.author.top_role > role_ids_to_roles(message.server, admin_baseline_role)[0]:
        p.endline(log_off_message)
        em = discord.Embed(title=self.name, description=log_off_message,
            color=color['success'])
        await client.send_message(message.channel, embed=em)
        await client.logout()
        if logger_enabled:
            logging.critical(message.author.name + " has STOPPED the bot")
        sys.exit("Stop command run")
    else:
        em = discord.Embed(title=self.name, description="Your current permissions"
            " don't allow you to stop the bot.", color=color['error'])
        await client.send_message(message.channel, embed=em)
        logging.critical(message.author.name + " has attempted to STOP the bot")

cmdStop = Cmd(
    name="STOP",
    description="**.stop:** For micromods+ only, used to shut down the bot until "
    "further notice. If a glitch is found where someone could get admin, delete "
    "info, or anything else, don't hesitate to use this command. It wont hurt the "
    "code and it could be started up again in a snap.",
    execution=cmd_execution
    )

# ==== LINK ===================================================================

async def cmd_execution(self, message, uIn):
    # if no arguments are given, just list all the links
    if uIn.args == []:
        temp = []
        for link in links_json:
            temp.append(link + ": <" + links_json[link] + ">")
        temp = "\n".join(temp)
        await send_pages(message.channel, member=message.author, self=self, message=temp, color=color['success'])
    # If a sneaky boy tries to add "ADD" as a link
    elif uIn.args[0][0] == "ADD":
        add_invalid_msg = "Invalid format"\
        " The correct format to add a link is `" + cmd_prefix + "link [add"\
        " [link name [link`"
        add_successful_msg = "Add successful"
        try:
             uIn.args[1]
             uIn.args[2]
        except IndexError:
            em = discord.Embed(title=self.name,
                description=add_invalid_msg, color=color['error'])
            await client.send_message(message.channel, embed=em)
        else:
            if uIn.args[1][0] == "ADD":
                em = discord.Embed(title=self.name,
                    description="You sly little booger", color=color['error'])
                await client.send_message(message.channel, embed=em)
            else:
                if uIn.args[2][0].startswith("HTTP://"):
                    link_url = uIn.args[2][0].lower()
                else:
                    link_url = "http://" + uIn.args[2][0].lower()
                links_json[uIn.args[1][0]] = link_url
                em = discord.Embed(title=self.name, description=add_successful_msg, color=color['successful'])
                await client.send_message(message.channel, embed=em)
    # Otherwise, look for the link and return it if found. If not, say so.
    else:
        for link in links_json:
            if uIn.args[0][0] == link:
                em = discord.Embed(
                    title=self.name,
                    description=link + ": " + links_json[link],
                    color=color['success']
                )
                await client.send_message(message.channel, embed=em)
                return
        em = discord.Embed(
            title=self.name,
            description="Link not found",
            color=color['error']
        )
        await client.send_message(message.channel, embed=em)

cmdLink = Cmd(
    name="LINK",
    description="**.link:** .link [name: Returns the link for the name listed \
    such as \".link [bb\" or \".link [BlackBoard\" returns the link for blackboard. \
    You can also add a link by using `.link [add [LINK_NAME [URL`. To get a list \
    of all the links, use `.link` (no arguments)",
    execution=cmd_execution
    )

# ==== NUKE ==================================================================

def check(message):
    if message.content.upper().startswith("Y") or message.content.upper().startswith("N"):
        return True
    else:
        return False

async def cmd_execution(self, message, uIn):
    await send_pages(
        channel=message.channel,
        member=message.author,
        message=(message.author.mention + " Nibba, you sure? This will wipe out every class role from every member. Respond yes/no or y/n")
        )
    answer = await client.wait_for_message(
        timeout=(60 * 5),
        author=message.author,
        channel=message.channel,
        check=check
        )
    if answer == None:
        await send_pages(
            channel=message.channel,
            member=message.author,
            message=(message.author.mention + " Silly goose, you didn't respond in time")
            )
    answer_content = answer.content.upper()
    if answer_content.startswith("Y"):
        await send_pages(
            channel=message.channel,
            member=message.author,
            message=(message.author.mention + " your funeral")
            )
        await NUKE()
    elif answer_content.startswith("N"):
        await send_pages(
            channel=message.channel,
            member=message.author,
            message=(message.author.mention + " be careful bibba.")
            )

cmdNuke = Cmd(
    name="NUKE",
    description="**.nuke:** Reserved for mods. It removes EVERY class role from "
    "EVERY member in the server."
    " Make sure you want to do this before you actually do it.",
    execution=cmd_execution
    )

# ==== DELETION ================================================================

del cmd_execution
# ==============================================================================
# EVENTS
# ==============================================================================


@client.event
async def on_ready():
    p.title("Bot Ready")

    global PVSG
    PVSG = client.get_server(PVSG_id)

    await sync_roles(PVSG)
    await msg_members_without_class_role()

    # global emojis
    # emojis = client.get_all_emojis()
    # p.debug(emojis)
    # p.debug(next(emojis))
    # get_emoji("thumbs_up")

    # Startup message to all connected servers
    em = discord.Embed(title="Online", color=color['success'], description="Bot online and ready to terminate all humans.")
    await message_all_servers(embed=em)

    # Using "playing" to have a custom game message for the bot
    game = discord.Game(name="Type '" + cmd_prefix + "help' to start")
    await client.change_presence(game=game)

    client.loop.create_task(async_loop())

@client.event
async def on_member_join(member):
    if member.user != client.user:
        await join(member)

@client.event
async def on_member_update(bef, aft):
    # Someone added or removed a role. I need to be able to pick out the items in the list
    # That are different from one another
    if bef.roles != aft.roles:
        # Has class role, continue (or has added class role)
        cr = has_class_role(aft)
        if cr:
            dif_role = find_different_item(bef.roles, aft.roles)
            # Get their grade and use the grade to append their role added to the correct field
            grade = class_to_grade(cr)
            # Role removed
            if len(bef.roles) > len(aft.roles):
                remove_role(aft.id, dif_role, grade)
            # Role added
            if len(bef.roles) < len(aft.roles):
                add_role(aft.id, dif_role, grade)
        else:
            pass
    with open("students.json", "w") as fw:
        json.dump(students_json, fw, indent=2)

@client.event
async def on_message(message):
    # p.debug(message.author, " ", client.user, " ", message.author != client.user)
    send_message_to_console(message)
    if message.author != client.user:
        await test_and_run_commands(message)

# @client.event
# async def on_error(event, *args, **kwargs):
#     p.debug(event)

# ==============================================================================
# RUNNING SCRIPT
# ==============================================================================

# Storing private token in another file that isn't uploaded
token = open("token.txt", "r").readline().strip("\n")
client.run(token)

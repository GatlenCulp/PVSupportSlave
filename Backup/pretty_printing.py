from inspect import stack
from colorama import Fore, Back, Style

def endline(*args):
    # Prints in a special format
    statements = []
    for arg in args:
        if type(arg) == str:
            statements.append(arg.upper())
        else:
            statements.append(arg)

    print(Fore.LIGHTWHITE_EX, Back.RED, "\t^^^^^^^^^^ ", *statements, " ^^^^^^^^^^\n", Style.RESET_ALL)

def debug(*args, title=None):
    # Prints in a special format to more easily find a printed statement for debugging
    # The stack portion grabs the line number in the code of the printed message and the file name
    scope = stack()[1]
    if title == None:
        print("\n\033[4m\033[31m\tLINE NUMBER: {} | FILE NAME: {}\n\033[0m\033[31m".format(scope.lineno, scope.filename), *args, "\033[0m\n")
    else:
        print("\n\033[4m\033[31m{}\tLINE NUMBER: {} | FILE NAME: {}\n\033[0m\033[31m".format(title, scope.lineno, scope.filename), *args, "\033[0m\n")


def title(*args):
    # Prints in a special format
    statements = []
    for arg in args:
        if type(arg) == str:
            statements.append(arg.upper())
        else:
            statements.append(arg)

    print("\033[1m\033[37m\033[42;1m\n\t-----=====", *statements, " =====-----\033[0m")

def long(string):
    prev_char_was_space = False
    prev_char_was_nl = False
    for char in string:
        if char == "\n":
            if prev_char_was_nl == False:
                char = ""
            prev_char_was_nl = True
        else:
            prev_char_was_nl = False
            if char == " ":
                if prev_char_was_space == False:
                    char = ""
                prev_char_was_space = True
            else:
                prev_char_was_space = False
    return string

def long(str):
    string = str
    prev_char_was_space = False
    prev_char_was_nl = False
    for char in string:
        if char == "\n":
            if prev_char_was_nl == False:
                char = ""
            prev_char_was_nl = True
        else:
            prev_char_was_nl = False
            if char == " ":
                if prev_char_was_space == False:
                    char = ""
                prev_char_was_space = True
            else:
                prev_char_was_space = False
    return string

if __name__ == "__main__":
    endline("End line test", "big boy", type("yes sir"), 12)
    debug("Just you know, testing the debug command right now", title="Debug test")
    title("Title test")
    print(
    '''
        *So, on the PV Support Group server we use roles to tag people with
        what classes they have.* Why would we do this? Because you can mention
        everyone with a role by doing **@RoleName**

        Example (it wont work in DMs bc there aren't any roles here):
        @algebra II – robertson.
    '''
    )
    print(
    long(
    '''
        *So, on the PV Support Group server we use roles to tag people with
        what classes they have.* Why would we do this? Because you can mention
        everyone with a role by doing **@RoleName**

        Example (it wont work in DMs bc there aren't any roles here):
        @algebra II – robertson.
    '''
)
)
